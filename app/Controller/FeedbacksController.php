<?php
App::uses('AppController', 'Controller');

class FeedbacksController extends AppController {
    public $uses = array('User','Feedback');

    public $layout = 'main';
 
    public function beforeFilter() {
        parent::beforeFilter();
        if($my_datas = $this->Auth->User()) {
           $this->Session->write('my_datas', $my_datas);
        }
    
    }

    public function addFeedBack() {
        $my_datas = $this->Session->read('my_datas');

        if($this->request->is('post')) {
           $comment = $this->request->data['Feedback']['comment']; 

           $feedback_data = array(
                'Feedback' => array(
                    'user_id' => $my_datas['id'],
                    'comment' => $comment,
                )
            );

           if($this->Feedback->save($feedback_data)) {
              $this->set(compact('my_datas'));
              return $this->render('success');
           }
        }

        $this->set(compact('my_datas'));
        $this->render('add_feedback');
    }
       
}

?>