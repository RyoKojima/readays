<?php
App::uses('AppController', 'Controller');

class MainsController extends AppController {
	
    public $uses = array('User','Color', 'Question', 'QuestionResponse');

    public $layout = 'main';

    public function beforeFilter() {
        parent::beforeFilter();

        if($my_datas = $this->Auth->User()) {
            $this->Session->write('my_datas', $my_datas);
        }

        $this->Auth->allow('index', 'profileShow');

    }

    public $paginate = array(
        'Question' => array(
            'limit' => 8,
            'order' => 'Question.created DESC',
        ),
        'Answere' => array(
            'limit' => 6,
            'order' => 'Answere.created DESC',
        ),
    );

    public function index() {
        $my_datas  = $this->Session->read('my_datas');            
        $questions = $this->paginate('Question');

        $this->set('my_datas', $my_datas);
        $this->set('questions', $questions);
        $this->render('index');
    }

    public function profileShow() {
        if($this->request->is('get')) {

            $my_datas  = $this->Session->read('my_datas');
            $nickname  = $this->request->params['named']['nn'];

            $this->User->contain();
            $users = $this->User->find('first', array(
                'conditions' => array(
                    'User.nickname' => $nickname,
                ),
                'fields'     => array('User.id', 'User.nickname','User.name', 'User.picture'
                ) 
            ));

            $this->Answere->contain('User', 'Color', array('Supporter' => array('User')), 'Question');
            $answeres = $this->paginate('Answere', array(
                'Answere.user_id' => $users['User']['id'],
            ));

            if(empty($users)) {

                return $this->render('error_404');
            }

            foreach ($answeres as $key) {
                $time_db  = $key['Answere']['created'];
                $key['Answere']['created'] = $this->convert_to_fuzzy_time($time_db);

                $ans_data[] = $key;
            }


            $this->set('my_datas', $my_datas);
            $this->set(compact('users'));
            $this->set(compact('ans_data'));

            $this->render('profile_show');
        }
    }

    public function ansDelete() {
        if($this->request->is('get')) {
            $my_datas      = $this->Session->read('my_datas');
            $question_id   = $this->request->params['named']['qd'];
            $ans_id        = $this->request->params['named']['ad'];

            $question_check = $this->Question->find('first', array(
                'conditions' => array(
                    'Question.id'      => $question_id,
                )
            ));

            $ans_check = $this->Answere->find('first', array(
                'conditions' => array(
                    'Answere.id'      => $ans_id,
                    'Answere.user_id' => $my_datas['id']
                )
            ));

            if(empty($ans_check) || empty($question_check)) {
                return $this->render('error_404');
            }

            if($this->Answere->delete($ans_check['Answere']['id'])) {

                $this->redirect(array(
                    'controller' => 'Mains',
                    'action'     => 'profileShow',
                    'sd'         => $my_datas['social_id'],
                ));
            }
        }
    }
}
?>