<?php
App::uses('AppController', 'Controller');

class ResponsesController extends AppController {
    public $uses = array('QuestionResponse', 'Question');

    public $layout = 'main';
 
    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function responseCheck() {
        if($this->request->is('get')) {
            $my_datas    = $this->Session->read('my_datas');
            $response_id = $this->request->params['named']['rd'];

            $response_opt = array(
                'conditions' => array(
                    'QuestionResponse.id'          => $response_id,
                    'QuestionResponse.receiver_id' => $my_datas['id'],
                    'QuestionResponse.check'       => 0,
                )
            );

            $response_check = $this->QuestionResponse->find('first', $response_opt);

            if(empty($response_check)) {
                return $this->render('error_404');
            }

            $response_data = array(
                'QuestionResponse.check' => 1,
            );

            $conditions = array(
                'QuestionResponse.receiver_id' => $response_check['QuestionResponse']['receiver_id'],
                'QuestionResponse.question_id' => $response_check['QuestionResponse']['question_id']
            );

            if($this->QuestionResponse->updateAll($response_data, $conditions)) {
                return $this->redirect(array(
                    'controller' => 'Questions',
                    'action'     => 'detailQuestion',
                    'qd'         => $response_check['QuestionResponse']['question_id']
                ));
            }
        }
    }
 
}
?>