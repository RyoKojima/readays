<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {
	
    public $uses = array('User', 'FacebookAccount', 'TwitterAccount');

    public $layout = 'Landing';

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('opauthComplete', 'afterDelete');

        if($this->params['action'] == 'opauthComplete') {
            $this->Security->csrfCheck    = false;
            $this->Security->validatePost = false;
        }
    }

    public function opauthComplete() {
        $opauth_data = $this->data;
       
        if(!empty($opauth_data['error'])) {
            return $this->redirect(array(
                'controller' => 'Mains',
                'action'     => 'index'
            ));
        }
        
        $user_check  = $this->_socialCheck($opauth_data);

        if(empty($user_check)) {
            $user_data = array(
                'User' => array(
                    'nickname' => $this->data['auth']['info']['nickname'],
                    'name'     => $this->data['auth']['info']['name'],
                    'picture'  => $this->data['auth']['info']['image']
                )
            );

            if($this->User->save($user_data)){

                $user_opt = array(
                    'conditions' => array(
                       'User.nickname' => $this->data['auth']['info']['nickname'],
                    )
                );

                $this->User->contain();
                $user = $this->User->find('first', $user_opt);

                if($this->data['auth']['provider'] === 'Twitter') {

                    $TwitterAccount_data = array(
                        'TwitterAccount' => array(
                            'user_id'    => $user['User']['id'],
                            'social_uid' => $this->data['auth']['uid']
                        )
                    );

                    $this->TwitterAccount->save($TwitterAccount_data);

                } elseif($this->data['auth']['provider'] === 'Facebook') {

                    $FacebookAccount_data = array(
                        'FacebookAccount' => array(
                            'user_id'    => $user['User']['id'],
                            'social_uid' => $this->data['auth']['uid']
                        )
                    );

                    $this->FacebookAccount->save($FacebookAccount_data);
                }

                $user_info = array(
                    'id'        => $user['User']['id'],
                    'nickname'  => $user['User']['nickname'],
                    'name'      => $user['User']['name'],
                    'picture'   => $user['User']['picture'],
                );

                if($this->Auth->login($user_info)) {
                    return $this->redirect(array(
                      'controller' => 'Mains',
                      'action'     => 'index'
                    ));
                }

            }

        } else {

            // 既にユーザーデータがあれば、Authに通してログイン
            $user_info = array(
                'id'        => $user_check['User']['id'],
                'nickname'  => $user_check['User']['nickname'],
                'name'      => $user_check['User']['name'],
                'picture'   => $user_check['User']['picture'],
            );

            if($this->Auth->login($user_info)) {

                return $this->redirect(array(
                    'controller' => 'Mains',
                    'action'     => 'index'
                ));
            } 
        }
    }

    public function _socialCheck($opauth_data) {

        if($opauth_data['auth']['provider'] === 'Twitter') {

            $TwitterAccount_opt = array(
                'conditions' => array(
                    'social_uid' => $opauth_data['auth']['uid']
                )
            );

            $this->TwitterAccount->contain('User');
            $user_check = $this->TwitterAccount->find('first', $TwitterAccount_opt);

            return $user_check;

        } elseif($opauth_data['auth']['provider'] === 'Facebook') {

            $FacebookAccount_opt = array(
                'conditions' => array(
                    'social_uid' => $opauth_data['auth']['uid']
                )
            );

            $this->FacebookAccount->contain('User');
            $user_check = $this->FacebookAccount->find('first', $FacebookAccount_opt);

            return $user_check;
        }

    }

    public function logOut() {
        $_SESSION = array();
        session_destroy();
        return $this->redirect(array('controller' => 'Startes', 'action' => 'index'));
    }

    public function setingShow() {
        if($this->request->is('get')) {
            $this->layout = 'main';
            $my_datas = $this->Session->read('my_datas');

            $this->set('my_datas', $my_datas);
            $this->render('seting_show');
        }
    }

    public function accountDelete() {
        if($this->request->is('get')) {
            $my_datas = $this->Session->read('my_datas');

            if($this->User->delete($my_datas['id'])) {
                return $this->redirect('afterDelete');
            } else {
                return $this->redirect('setingShow');
            }
        }
    }

    public function afterDelete() {
        $_SESSION = array();
        session_destroy();
        $this->render('after_delete');
    }
}
?>