<?php

App::uses('AppController', 'Controller');

class QuestionsController extends AppController {
    public $uses = array('Question', 'Answere');

    public $layout = "main";

    public function beforeFilter() {
        parent::beforeFilter();

        $this->Auth->allow('detailQuestion');
    }

    public $paginate = array(
        'Question' => array(
            'limit' => 8,
            'order' => 'Question.created DESC',
        ),
        'Answere' => array(
            'limit' => 6,
            'order' => 'Answere.created DESC',
        ),
    );

    public function addQuestion() {
        $my_datas = $this->Session->read('my_datas');
      
        if($this->request->is('post')) {
            $question_data = array(
                'Question' => array(
                    'user_id'   => $my_datas['id'],
                    'anonymity' => $this->request->data['Question']['anonymity'],
                    'content'   => $this->request->data['Question']['content'],
                )
            );

            if($this->Question->save($question_data)) {

                $this->redirect(array(
                    'controller' => 'Mains',
                    'action'     => 'index'
                ));
            } else {

                $this->Session->setFlash('質問の投稿に失敗しました。');
                $this->redirect(array(
                    'controller' => 'Mains',
                    'action'     => 'index'
                ));
            }
        }
    }

    public function detailQuestion() {
        if($this->request->is('get')) {
            $my_datas    = $this->Session->read('my_datas');
            $question_id = $this->request->params['named']['qd'];

            $this->Question->contain('User');
            $questions = $this->Question->find('first', array(
                'conditions' => array(
                    'Question.id' => $question_id,
                )
            ));

            $this->Answere->contain('User', 'Color', array('Supporter' => array('User')));
            $answeres = $this->paginate('Answere', array(
                'Answere.question_id' => $question_id,
            ));

            if(!empty($answeres)) {
                // アンサー配列にサポーターかの判別のため、配列を追加。デフォルトとしてfalseを追加しておく。
                foreach ($answeres as $key) {
                    
                    $key['Answere'] += array(
                        'support_flag' => 'false');

                    $time_db  = $key['Answere']['created'];
                    $key['Answere']['created'] = $this->convert_to_fuzzy_time($time_db);
                    
                    $flag_check[] = $key;
                }

                // SupporterデータのユーザーIDと自分のIDを比較することで、既にサポートしているかの判別を行う。そしてAnsデータを出力する。
                foreach ($flag_check as $key) {
                    if(!empty($key['Supporter']) && !empty($my_datas)) {

                        foreach ($key['Supporter'] as $flag) {
                            if($flag['user_id'] === $my_datas['id']) {

                               $key['Answere']['support_flag'] = 'true';
                            }

                        }

                        $ansData[] = $key;
                        
                    } else {
                        $ansData[] = $key;
                    }
                }

            }
           
            if(empty($questions)) {
                return $this->render('404_error');
            } else {

                $this->set('my_datas', $my_datas);
                $this->set(compact('questions'));
                $this->set(compact('ansData'));
                $this->render('detail_question');
            }

        }
    }

    public function myQuestionList() {
        if($this->request->is('get')) {
            $my_datas = $this->Session->read('my_datas');

            $questions = $this->paginate('Question', array(
                'Question.user_id' => $my_datas['id']
            ));

            $this->set('my_datas', $my_datas);
            $this->set('questions', $questions);
            $this->render('my_question');
        }
    }

    public function delete() {
        if($this->request->is('get')) {
            $my_datas    = $this->Session->read('my_datas');
            $question_id = $this->request->params['named']['qd'];

            $questions = $this->Question->find('first', array(
                'conditions' => array(
                    'Question.id'      => $question_id,
                    'Question.user_id' => $my_datas['id']
                )
            ));

            if(empty($questions)) {
                $this->set('my_datas', $my_datas);
                return $this->render('error_404');
            }

            if($this->Question->delete($questions['Question']['id'])) {

                $this->redirect('myQuestionList');
            }
        }
    }
}
