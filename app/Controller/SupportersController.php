<?php

App::uses('AppController', 'Controller');

class SupportersController extends AppController {
    public $uses = array('Supporter', 'Answere');

    public $layout = "main";

    public function beforeFilter() {
      parent::beforeFilter();

      if($my_datas = $this->Auth->User()) {
         $this->Session->write('my_datas', $my_datas);
      }  
    }

    public $paginate = array(
      'Book' => array(
        'limit' => 12,
        'order' => 'Book.created DESC',
      )
    );

    public function addSupporter() {
        if($this->request->is('ajax')) {
            $my_datas = $this->Session->read('my_datas');
            $ans_id   = $_GET['ans_id'];

            $ans_check = $this->Answere->find('first', array(
                'conditions' => array(
                    'Answere.id' => $ans_id,
                )
            ));

            $support_data = array(
                'Supporter' => array(
                    'answere_id' => $ans_check['Answere']['id'],
                    'user_id'    => $my_datas['id'],
                )
            );

            if($this->Supporter->save($support_data)) {
                $this->set('ans_id', $ans_id);
                $this->render('add_supporter', 'ajax');
            }
        }
    }

    public function removeSupporter() {
        if($this->request->is('ajax')) {
            $my_datas = $this->Session->read('my_datas');
            $ans_id   = $_GET['ans_id'];

            $supporter_opt = array(
                'conditions' => array(
                    'answere_id' => $ans_id,
                    'user_id'    => $my_datas['id'],
                )
            );

            $supporter_check = $this->Supporter->find('first', $supporter_opt);

            if(empty($supporter_check)) {
                return;
            }

            if($this->Supporter->delete($supporter_check['Supporter']['id'])) {

                $this->set('ans_id', $ans_id);
                $this->render('remove_supporter', 'ajax');
            }
        }

    }

}
