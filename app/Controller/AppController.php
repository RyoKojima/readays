<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $facebook;

    public $helper = array('Html', 'Form', 'Session');

    public $uses = array('QuestionResponse', 'Answere');

	public $components = array(
        'Session',
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'Startes',
                'action'     => 'index'
            ),
            'authenticate'  => array(
                'Form' => array(
                    'fields' => array(
                        'username' => 'email', 
                        'password' => 'password',
                    )
                )
            ),
        ),
        'Security',
        'DebugKit.Toolbar'
    );

    public function beforeFilter() {

        if($my_datas = $this->Auth->User()) {
            $this->Session->write('my_datas', $my_datas);

            $response_opt = array(
                'conditions' => array(
                    'QuestionResponse.receiver_id' => $my_datas['id'],
                    'QuestionResponse.check'   => 0
                ),
                'fields' => array('QuestionResponse.id', 'QuestionResponse.message', 'QuestionResponse.created'),
                'order'  => array('QuestionResponse.created desc')
            );

            $this->QuestionResponse->contain('User', 'Question');
            $question_res = $this->QuestionResponse->find('all', $response_opt);

            if(!empty($question_res)) {
                foreach ($question_res as $key) {
                    $time_db  = $key['QuestionResponse']['created'];
                    $key['QuestionResponse']['created'] = $this->convert_to_fuzzy_time($time_db);

                    $res_link_data[] = $key;
                }
            } else {

                $res_link_data = array();
            }

            $this->set(compact('res_link_data'));
        }  

       
    }

    public function convert_to_fuzzy_time($time_db){
        $unix   = strtotime($time_db);
        $now    = time();
        $diff_sec   = $now - $unix;
     
        if($diff_sec < 60){
            $time   = $diff_sec;
            $unit   = "秒前";
        }
        elseif($diff_sec < 3600){
            $time   = $diff_sec/60;
            $unit   = "分前";
        }
        elseif($diff_sec < 86400){
            $time   = $diff_sec/3600;
            $unit   = "時間前";
        }
        elseif($diff_sec < 2764800){
            $time   = $diff_sec/86400;
            $unit   = "日前";
        }
        else{
            if(date("Y") != date("Y", $unix)){
                $time   = date("Y年n月j日", $unix);
            }
            else{
                $time   = date("n月j日", $unix);
            }
     
            return $time;
        }
     
        return (int)$time .$unit;
    }

}
