<?php

App::uses('AppController', 'Controller');

class StocksController extends AppController {
    public $uses = array('Stock');

    public $layout = "main";

    public function beforeFilter() {
      parent::beforeFilter();

      if($my_datas = $this->Auth->User()) {
         $this->Session->write('my_datas', $my_datas);
      }  
    }

    public $paginate = array(
        'Stock' => array(
            'limit' => 6,
            'order' => 'Stock.created DESC',
        )
    );


    public function stockShow() {
        $my_datas = $this->Session->read('my_datas');

        $this->Stock->contain(
            array(
                'Answere' => array(
                    'User', 
                    'Color', 
                    'Stock', 
                    'Supporter'
                )
            )
        );

        $stocks = $this->paginate('Stock', array(
            'Stock.user_id' => $my_datas['id']
        ));

        if(!empty($stocks)) {
            // アンサーの投稿時からの相対時間を収得する
            foreach ($stocks as $key) {
                $time_db = $key['Answere']['created'];
                $key['Answere']['created'] = $this->convert_to_fuzzy_time($time_db);

                $stock_data[] = $key;
            }
        }

        $this->set('my_datas', $my_datas);
        $this->set(compact('stock_data'));
        $this->render('stock_show');
    }

    public function remove() {
        if($this->request->is('get')) {
            $my_datas = $this->Session->read('my_datas');
            $stock_id  = $this->request->params['named']['std'];

            $stock_opt = array(
                'conditions' => array(
                    'Stock.id' => $stock_id,
                    'Stock.user_id' => $my_datas['id']
                )
            );

            $stock_check = $this->Stock->find('first', $stock_opt);

            if(empty($stock_check)) {
                // 404エラー
                return $this->render('error_404');
            }

            if($this->Stock->delete($stock_check['Stock']['id'])) {

                return $this->redirect(array(
                    'controller' => 'Stocks',
                    'action'     => 'stockShow')
                );
            }

        }
    }

   
}
