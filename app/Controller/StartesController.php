<?php
App::uses('CakeEmail', 'Network/Email');

class StartesController extends AppController {
	
  public $uses = array('User');

  public $layout = 'Landing';

  public function beforeFilter() {
    parent::beforeFilter();
    
    $this->Auth->allow('index', 'profileShow');
  }

  public function index () {
    $this->User->contain();
    $recommend_user = $this->User->find('all', 
        array(
            'limit' => 7, 
            'fields' => array('User.nickname', 'User.picture')
        )
    );
    
    $this->set(compact('recommend_user'));
    $this->render('landing_show');
  }

  /* public function addEmail() {
    $email = new CakeEmail();

         $email->transport('Mail');

         $email->from('m0111157ad@st.teu.ac.jp');
         $email->to('m0111157ad@st.teu.ac.jp');

         $email->subject('これはテストメールです。');
         $message = $email->send('これはテストメールの本文です');

         $this->set('message', $message);
         $this->render('send_debug');
   
  }*/

}
?>