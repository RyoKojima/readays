<?php
App::uses('AppController', 'Controller');

class AnsweresController extends AppController {
    public $uses = array('User','Answere', 'Question', 'Color', 'QuestionResponse');

    public $layout = 'main';
 
    public function beforeFilter() {
        parent::beforeFilter();
        if($my_datas = $this->Auth->User()) {
           $this->Session->write('my_datas', $my_datas);
        }

        if($this->params['action'] == 'bookSearch') {
           $this->Security->csrfCheck = false;
           $this->Security->validatePost = false;
        }
    
    }

    public function bookSearch() {
        if($this->request->is('ajax')) {
            $book_xml = $this->amzRequest($_POST['words']);
            $question_id = $_POST['qd'];

            $this->set(compact('book_xml'));
            $this->set('qd', $question_id);
            $this->render('book_search', 'ajax');
        }
    }

    public function ansForm() {
        if($this->request->is('get')) {
            $my_datas     = $this->Session->read('my_datas');
            $question_id  = $_GET['qd'];
            $get_words    = $_GET['asin'];

            $this->Question->contain();
            $questions = $this->Question->find('first', array(
                'conditions' => array(
                    'Question.id' => $question_id
                )
            ));

            if(empty($questions)) {
                return $this->render('error_404', 'ajax');
            }

            $book_xml   = $this->amzRequest($get_words);
            $color_list = $this->Color->find('list');

            $this->set(compact('book_xml'));
            $this->set(compact('questions'));
            $this->set('color_list', $color_list);
            $this->render('ans_form', 'ajax');
        }
    }

    public function addAns() {
        $my_datas = $this->Session->read('my_datas');

        if($this->request->is('post')) {
            $question_id = $this->request->data['Answere']['qd'];
            $phrase      = $this->request->data['Answere']['phrase'];
            $page_number = $this->request->data['Answere']['page_number'];
            $book_img    = $this->request->data['Answere']['book_img'];
            $book_detail = $this->request->data['Answere']['book_detail'];
            $color_id    = $this->request->data['Answere']['color'];

            $question_opt = array(
                'conditions' => array(
                    'Question.id' => $question_id
                ),
                'fields' => array('Question.id', 'Question.user_id')
            );

            $question_check = $this->Question->find('first', $question_opt);

            if(empty($question_check)) {
                return $this->render('error_404');
            }

            $ans_data = array(
                'Answere' => array(
                    'user_id'     => $my_datas['id'],
                    'question_id' => $question_check['Question']['id'],
                    'color_id'    => $color_id,
                    'phrase'      => $phrase,
                    'page_number' => $page_number,
                    'book_img'    => $book_img,
                    'book_detail' => $book_detail
                )
            );

            if($this->Answere->save($ans_data)) {

                $response_data = array(
                    'QuestionResponse' => array(
                        'receiver_id' => $question_check['Question']['user_id'],
                        'sender_id'   => $my_datas['id'],
                        'question_id' => $question_check['Question']['id'],
                        'message'     => $my_datas['name'].'さんがあなたの質問にオススメを投稿しました!',
                    )
                );

                $this->QuestionResponse->save($response_data);

                $this->Session->setFlash('オススメの投稿に成功しました。');
                return $this->redirect(array(
                    'controller' => 'Questions',
                    'action'     => 'detailQuestion',
                    'qd'         => $question_id
                ));
              
            } else {
                $this->Session->setFlash('オススメの投稿に失敗しました。');
                return $this->redirect(array(
                    'controller' => 'Questions',
                    'action'     => 'detailQuestion',
                    'qd'         => $question_id
                ));
            }
        }
    }

    public function ansDelete() {
        if($this->request->is('get')) {
            $my_datas      = $this->Session->read('my_datas');
            $question_id   = $this->request->params['named']['qd'];
            $ans_id        = $this->request->params['named']['ad'];

            $question_check = $this->Question->find('first', array(
                'conditions' => array(
                    'Question.id'      => $question_id,
                )
            ));

            $ans_check = $this->Answere->find('first', array(
                'conditions' => array(
                    'Answere.id'      => $ans_id,
                    'Answere.user_id' => $my_datas['id']
                )
            ));

            if(empty($ans_check) || empty($question_check)) {
                return $this->render('error_404');
            }

            if($this->Answere->delete($ans_check['Answere']['id'])) {

                $this->redirect(array(
                    'controller' => 'Questions',
                    'action'     => 'detailQuestion',
                    'qd'         => $question_check['Question']['id']
                ));
            }
        }
    }

    public function amzRequest($get_words) {
  
        function urlencode_rfc3986($str) {
          return str_replace('%7E', '~', rawurlencode($str));
        }

       if(isset($get_words)){

          $access_key_id='AKIAJIH75S4RKVPTXLZA';
          $secret_access_key = 'Ju1xpfS7wi5oJ+99YGZXOsMz35mQCti9dZvx1VHf';

          $words = h($get_words);

          $baseurl = 'http://ecs.amazonaws.jp/onca/xml';

          $params = array();
          $params['Service']        = 'AWSECommerceService';
          $params['AWSAccessKeyId'] = $access_key_id;
          $params['Version']        = '2011-08-01';
          $params['Operation']      = 'ItemSearch'; // ← ItemSearch オペレーションの例
          $params['SearchIndex']    = 'Books';
          $params['Keywords']       = $words;     // ← 文字コードは UTF-8
          $params['AssociateTag']   = 'bookfit-22';
          $params['ResponseGroup']  = 'ItemAttributes,Images';

          // Timestamp パラメータを追加します
          // - 時間の表記は ISO8601 形式、タイムゾーンは UTC(GMT)
          $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');

          // パラメータの順序を昇順に並び替えます
          ksort($params);

          // canonical string を作成します
          $canonical_string = '';
          foreach ($params as $k => $v) {
              $canonical_string .= '&'.urlencode_rfc3986($k).'='.urlencode_rfc3986($v);
          }

          $canonical_string = substr($canonical_string, 1);

          // 署名を作成します
          // - 規定の文字列フォーマットを作成
          // - HMAC-SHA256 を計算
          // - BASE64 エンコード
          $parsed_url = parse_url($baseurl);
          $string_to_sign = "GET\n{$parsed_url['host']}\n{$parsed_url['path']}\n{$canonical_string}";
          $signature = base64_encode(hash_hmac('sha256', $string_to_sign, $secret_access_key, true));

          // URL を作成します
          // - リクエストの末尾に署名を追加
          $url = $baseurl.'?'.$canonical_string.'&Signature='.urlencode_rfc3986($signature);
          /*echo $url;*/

          //XMLファイルをパースし、オブジェクトを取得
          $xml = simplexml_load_file($url)or die("XMLパースエラー");

          return $xml;
    }
  }
       
}

?>