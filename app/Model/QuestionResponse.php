<?php

App::uses('AppModel', 'Model');

class QuestionResponse extends AppModel {
	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'sender_id',
			'fields'     => array('User.picture', 'User.name')
		),
		'Question' => array(
			'foreignKey' => 'question_id',
			'fields'     => array('Question.id')
		)
	);
}
