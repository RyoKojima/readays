<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

	public $hasMany = array(
		'Question' => array(
			'foreignKey' => 'user_id',
			'dependent'  => true,
		),
		'QuestionResponse' => array(
			'foreignKey' => 'receiver_id',
			'dependent'  => true,
		),
		'Answere' => array(
			'foreignKey' => 'user_id',
			'dependent'  => true,
		),
		'Supporter' => array(
			'foreignKey' => 'user_id',
			'dependent'  => true,
		),
		'FacebookAccount' => array(
			'foreignKey' => 'user_id',
			'dependent'  => true,
		),
		'TwitterAccount' => array(
			'foreignKey' => 'user_id',
			'dependent'  => true,
		),
	);

	public function beforeSave($options = array()) {
		if(!empty($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}
}
?>