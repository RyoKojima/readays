<?php

App::uses('AppModel', 'Model');

class Feedback extends AppModel {

	public $validate = array(
	    'comment' => array(
	      array(
	        'rule' => 'notEmpty',
	        'message' => 'コメントが入力されていません。',
	      )
	    ),
  	);
	
}
