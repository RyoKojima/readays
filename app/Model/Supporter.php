<?php

App::uses('AppModel', 'Model');

class Supporter extends AppModel {
	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'user_id',
			'fields'     => array('User.nickname','User.picture')
		),
	);

}
