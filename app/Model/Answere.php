<?php


App::uses('AppModel', 'Model');

class Answere extends AppModel {

	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'user_id',
			'fields'     => array('User.nickname','User.name','User.picture')
		),
		'Color' => array(
			'foreignKey' => 'color_id',
			'fields'     => array('Color.color_name')
		),
		'Question' => array(
			'foreignKey' => 'question_id',
			'fields'     => array('Question.id', 'Question.content'),
		),
	);

	public $hasMany = array(
		'Supporter' => array(
			'foreignKey' => 'answere_id',
			'fields'     => array('Supporter.id', 'Supporter.user_id'),
			'dependent'  => true,
		)
	);

	public $validate = array(
	    'phrase' => array(
	      array(
	        'rule' => 'notEmpty',
	      )
	    ),
	    'page_number' => array(
	      array(
	        'rule' => 'notEmpty',
	      )
	    )
  	);
}
