<?php

App::uses('AppModel', 'Model');

class Question extends AppModel {

	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'user_id',
			'fields'     => array('User.nickname', 'User.picture', 'User.name')
		),
	);

	public $hasMany = array(
		'Answere' => array(
			'foreignKey' => 'question_id',
			'fields'     => array('Answere.id', 'Answere.book_img'),
			'order'      => array('Answere.created desc'),
			'limit'      => 4,
			'dependent'  => true,
		)
	);
	
	public $validate = array(
	    'content' => array(
	    	array(
	        	'rule'    => 'notEmpty',
	      	),
	    )
  	);
}
