<?php

App::uses('AppModel', 'Model');

class TwitterAccount extends AppModel {
	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'user_id',
			'fields'     => array('User.id', 'User.nickname', 'User.name', 'User.picture')
		)
	);
}
