<div class="container text-center">
    <p class="lead">アカウントの削除</p>
    <p>アカウントを削除すると、全てのデータが削除されます。<br>Readaysを退会しますか?</p>
    <?php 
        echo $this->Html->link('Redaysを退会します。', 
            array(
               'controller' => 'Users',
               'action'     => 'accountDelete',
            ),
            array(
               'class'   => 'btn btn-danger',
               'confirm' => 'Redaysを退会します。よろしいですか?',
            )
        );
    ?>
</div>