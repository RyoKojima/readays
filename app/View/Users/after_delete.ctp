<div id="wrapper" class="container">
    <div id="main_content" class="container text-center">
        <div class="form-signin text-center">
            <p class="lead">Readaysのご利用ありがとうございました。</p>
            <?php 
                echo 
                $this->Html->link('Readaysから出る',
                    array(
                        'controller' => 'Startes', 
                        'action' => 'index'
                    ),
                    array(
                        'class' => 'btn btn-primary'
                    )
                );
            ?>
        </div>
    </div>
</div>