<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> レスポンス</a>
<ul class="dropdown-menu">
<?php if(empty($question_res)):?>
  <li class="divider"></li>
    <li><a href='#'>お知らせはありません。</a></li>
  <li class="divider"></li>
<?php else: ?>
  <?php foreach ($question_res as $key):?>
    <li class="divider"></li>
      <li>
        <?php 
            echo $this->Html->link($key['QuestionResponse']['message'],
              array(
                'controller' => 'Questions',
                'action'     => 'detailQuestion',
                'qd'         => $key['QuestionResponse']['question_id']
              )
            );
        ?>
      </li>
    <li class="divider"></li>
  <?php endforeach;?>
<?php endif;?>
</ul>