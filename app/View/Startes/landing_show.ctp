<div id="wrapper" class="container">
    <div class="col-md-12">
        <div id="main_content" class="container text-center">
            
            <div id="login_btns">
                <h3>あなたの悩みを解決する本を誰かがすぐに教えてくれる。<br>Q＆Aブックサービス</h3>

                <?php echo $this->Html->link('Login with Facebook',
                    array(
                        'controller' => 'auth', 
                        'action' => 'facebook'
                    ),
                    array(
                        'class' => 'btn btn-primary btn-lg')
                    );
                ?>

                <?php echo $this->Html->link('Login with Twitter',
                    array(
                        'controller' => 'auth', 
                        'action' => 'twitter'
                    ),
                    array(
                        'class' => 'btn btn-info btn-lg')
                    );
                ?>
            </div>

            <section>
                <h4>あなたのオススメの本は？</h4>
                <?php 
                    echo 
                    $this->Html->link('みんなの質問を見る',
                        array(
                            'controller' => 'Mains',
                            'action'     => 'index'
                        ),
                        array(
                            'class' => 'btn btn-link btn-lg')
                    );
                ?>
            </section>

            
            <div class="col-md-12 col-xs-12">
                <h4>誰のオススメを見る?</h4>

                <?php
                    foreach ($recommend_user as $key) {
                        echo $this->Html->link($this->Html->image($key['User']['picture'], array('class' => 'img-circle')),
                            array(
                                'controller' => 'Mains',
                                'action'     => 'profileShow',
                                'nn'         => $key['User']['nickname']
                            ),
                            array(
                                'escape' => false
                            )
                        );
                    }
                ?>
            </div>

        </div>
    </div>
</div>






  