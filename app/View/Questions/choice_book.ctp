<div id="book_result">
	<p>オススメする本を選択</p>
	<hr>
	<?php 
		if(!empty($books)){
		   	foreach ($books as $key) {
		   		echo '<span class="book-list">'.$this->Js->link(
		   			$this->Html->image($key['Book']['image_url']),
		   			array(
		   				'controller' => 'Answeres',
		   				'action'     => 'ansForm',
		   				'bd'         => $key['Book']['id'],
		   				'qd'         => $question_id,

		   			),
		   			array(
		   				'update'     => '#book_result',
		   				'escape'     => false,
		   				'buffer'     => false,
		   			)
		   		).'</span>';
		   		
		   	}	
		}

	?>
	<div id="page_nav">
	    <?php echo $this->Paginator->next('次のページ',array('class'=>'next')); ?>
	</div>
</div>