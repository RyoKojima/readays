
	<div class="text-center error-msg">
		<p><?php echo $this->Session->flash(); ?></p>
	</div>

	<div class="col-md-12">
		<div class="media">
			<?php 
				if($questions['Question']['anonymity'] == 0) {
					echo 
	                $this->Html->link(
	                    $this->Html->image($questions['User']['picture'],array('class' => 'img-circle')),
	                    array(
	                        'controller' => 'Mains',
	                        'action'     => 'profileShow',
	                        'sd'         => $questions['User']['nickname']
	                    ),
	                    array(
	                        'escape' => false,
	                        'class'  => 'pull-left rollover',
	                    )
	                );
            	}
            ?>

    		<div class="media-body">
        		
    			<p class="media-heading">
    				<?php 
    					if($questions['Question']['anonymity'] == 0) {
    						echo h($questions['User']['name']);
    					}
    				?>
    			</p>
        		<p><?php echo h($questions['Question']['content']);?></p>
    		</div>
    	</div><!-- media -->

    	<?php if(empty($my_datas)):?>
			<button class="btn btn-info btn-lg btn-block" data-toggle="modal" data-target="#loginLink">オススメする本の検索</button>
		<?php else:?>
			<button class="btn btn-info btn-lg btn-block" data-toggle="modal" data-target="#bookSearch">オススメする本の検索</button>
		<?php endif;?>
		<hr>
			
	</div>

	<?php if(!empty($ansData)):?>

		<div id="item_wrapper" class="clearfix">
		
			<?php foreach ($ansData as $key):?>
				<div class="col-md-4 col-sm-12 col-xs-12 item-box load-item">
					<div class="thumbnail ans-card <?php echo $key['Color']['color_name'];?>"> 
					    <p><?php echo 
						      	$this->Html->link(
							      	$this->Html->image($key['User']['picture'],array('class' => 'img-circle')),
							      	array(
							      		'controller' => 'Mains',
							      		'action'     => 'profileShow',
							      		'nn'         => $key['User']['nickname']
							      	),
							      	array(
							      		'escape' => false,
							      		'class'  => 'rollover user-unit',
							      	)
						      	);
						    ?>

						    <span class="pull-right"><?php echo $key['Answere']['created'];?></span>
						</p>

						<div class='card text-center'>
					    	<?php echo $this->Html->image($key['Answere']['book_img']);?>
					    </div>
					      
						<div class="caption text-center">
							
							<p><?php echo '<i class="fa fa-quote-right"></i> '.h($key['Answere']['phrase']);?></p>
							<p><?php echo '出典:'.h($key['Answere']['page_number']).'ページ';?></p>

							<hr>

							<div>
								<p>心に響いたユーザー</p>
								<?php 
									if(empty($key['Supporter'])) {
										echo '<h4>0人</h4>';
									} else {

										foreach ($key['Supporter'] as $supporter_data) {
											echo $this->Html->link(
												$this->Html->image($supporter_data['User']['picture'],
													array('class' => 'img-circle')
												),
												array(
			                                    	'controller' => 'Mains',
			                                    	'action'     => 'profileShow',
			                                    	'sd'         => $supporter_data['User']['nickname']
			                                	),
				                                array(
				                                    'escape' => false,
				                                    'class'  => 'rollover user-unit',
				                                )
				                            );
										}
									}
		                        ?>
							</div>

							<hr>

							<div class="tool">

								<?php if(empty($my_datas)):?>
									<a class="btn btn-default btn-sm" data-toggle="modal" data-target="#loginLink"><i class="fa fa-thumbs-up"></i></a>

								<?php else:?>

									<span class="support_box<?php echo $key['Answere']['id'];?>">
										<?php if($key['Answere']['support_flag'] === 'false'):?>

											<a class="add-supporter btn btn-default btn-sm" data-ad="<?php echo $key['Answere']['id'];?>"><i class="fa fa-heart"></i></a>	

										<?php else:?>

											<a class="remove-supporter btn btn-default btn-sm" data-ad="<?php echo $key['Answere']['id'];?>"><i class="fa fa-heart support-done"></i></a>

										<?php endif;?>
									</span>
								<?php endif;?>

								<a href="<?php echo h($key['Answere']['book_detail']);?>" class="btn btn-default btn-sm" target="_blank">Amazon</a>

								<?php 
									if($key['Answere']['user_id'] === $my_datas['id']) {
										echo $this->Html->link('<i class="fa fa-trash-o"></i>',
											array(
												'controller' => 'Answeres',
												'action'     => 'ansDelete',
												'qd'         => $questions['Question']['id'],
												'ad'         => $key['Answere']['id']
											),
											array(
												'escape' => false,
												'class'  => 'btn btn-default btn-sm',
												'confirm' => 'このカードを削除しますか?',
											)
										);
									}
								?>
							</div><!-- Tool -->

						</div>
					</div>
				</div>
			<?php endforeach;?>

			<div class="col-md-12 text-center">
		        <div class="page-nav">
		        	<?php echo $this->Paginator->next('次のページ',array('class'=>'next')); ?>
		    	</div>
			</div>
		</div>

	<?php else:?>
		<div class="col-md-12 text-center">
			<p>オススメの投稿はまだありません。</p>
		</div>
	<?php endif;?>

	

<!-- bookSearchModal -->
<div class="modal fade" id="bookSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">オススメする本の検索</h4>
      </div>
      <div class="modal-body">
      	
      		<?php 
			   echo $this->Form->create('book', 
			     array( 
			        'type'  => 'get',
			        'id'    => 'bookSearchForm', 
			        'class' => 'form-inline',
			        'role'  => 'form',
			      )
			    );
			?>
           
  			<div class="form-group col-ms-12 col-md-12">
			    <div class="input-group">
				    <?php
						echo $this->Form->input('words',
							array(
							   'type'        => 'text',
							   'label'       => false,
							   'div'         => false,
							   'class'       => 'form-control',
							   'placeholder' => 'キーワード',
							));

						echo $this->Form->input('qd',
							array(
							   'type'  => 'hidden',
							   'value' => $questions['Question']['id'],
							));
				    ?>
				    
				    <span class="input-group-btn">
				        <?php
				            echo $this->Js->submit('検索する',
				            array(
				                'update' => '#s_content',
				                'url'    => array(
				                   'controller' => 'Answeres',
				                   'action'     => 'bookSearch',
				                ),
				                'div'    => false,
				                'id'     => 'book_search',
				                'class'  => 'btn btn-success',
				            ));    
				        ?>
				    </span>
			    </div>
			</div>

			<?php echo $this->Form->end();?>

			<hr>

			<div id="s_content">
			</div>

  	  </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->