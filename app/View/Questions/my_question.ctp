<div class="col-md-12">
	<p class='lead col-md-12 text-center'>あなたの質問リスト</p>

    <?php if(empty($questions)):?>
        <p class="col-md-12 text-center">質問の投稿はまだありません。</p>

    <?php else:?>
        <div id="item_wrapper"> 
            <?php foreach ($questions as $key):?>
                <div class="item-box load-item col-md-4 col-sm-12 col-xs-12">
                    <div class="thumbnail">
                        <div class="media">
                            <?php 
                                if($key['Question']['anonymity'] == 0) {
                                    echo 
                                    $this->Html->link(
                                        $this->Html->image($key['User']['picture'],array('class' => 'img-circle')),
                                        array(
                                            'controller' => 'Mains',
                                            'action'     => 'profileShow',
                                            'sd'         => $key['User']['nickname']
                                        ),
                                        array(
                                            'escape' => false,
                                            'class'  => 'pull-left rollover',
                                        )
                                    );
                                }
                            ?>
                            <div class="media-body">
                                <div class="read-more">

                                    <p class="media-heading">
                                        <?php 
                                            if($key['Question']['anonymity'] == 0) {
                                                echo h($key['User']['name']);
                                            }
                                        ?>
                                    </p>

                                    <p><?php echo $this->Html->link(h($key['Question']['content']), array(
                                                'controller' => 'Questions',
                                                'action'     => 'detailQuestion',
                                                'qd'         => $key['Question']['id']
                                            ));?></p>
                                </div>
                            </div>
                        </div><!-- media -->
                        <div class="caption">

                            <div class="book-thum">
                                <?php foreach ($key['Answere'] as $ans){

                                    echo $this->Html->image($ans['book_img']);
                                }?>
                            </div>

                            <?php 
                                echo $this->Html->link('質問を削除する', array(
                                    'controller' => 'Questions',
                                    'action'     => 'delete',
                                    'qd'         => $key['Question']['id']
                                    ),
                                    array(
                                        'class' => 'btn btn-default btn-block',
                                        'confirm' => '質問を削除すると投稿されたオススメも削除されます。よろしいですか?',
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        
        <div class="page-nav">
            <?php echo $this->Paginator->next('次のページ',array('class'=>'next')); ?>
        </div>
    <?php endif;?>
</div>