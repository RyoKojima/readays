<div class="container">
	<div class="row">
		<div id="readay_box" class="col-md-7">
			<p class='lead'>リーデイ</p>
			<hr>
			<?php if(!empty($users['Readay'])):?>
				<?php foreach ($users['Readay'] as $key):?>
					<div class="media readay">
						<p><?php echo 
						      	$this->Html->link(
							      	$this->Html->image(
							      		$users['User']['picture'],array('class' => 'img-rounded')).' '.h($users['User']['name']),
							      	array(
							      		'controller' => 'Mains',
							      		'action'     => 'myReadayShow',
							      		'sd'         => $users['User']['social_id']
							      	),
							      	array(
							      		'escape' => false,
							      		'class'  => 'rollover user-unit',
							      	)
						      	);
						    ?>

						    <span class="pull-right"><?php echo $key['modified'];?></span>
						</p>

						<?php echo $this->Js->link(
						  $this->Html->image($key['Book']['image_url']),
						    array(
						        'controller' => 'Books',
						        'action'     => 'detailShow',
						        'bd'         => $key['Book']['id'],
						    ),
						    array(
						        'update' => '#readay_box',
						        'escape' => false,
						        'class'  => 'rollover pull-left',
						    )
						  );
						?>
						<div class="media-body">
							<div class="read-more">
						    	<h4 class="media-heading"><?php echo h($key['Theme']['theme']);?></h4>
						    	<p><?php echo h($key['content']);?></p>
							</div>
						</div>
						<div class="col-md-12 pull-right text-right">
							<?php 
						    	if($key['user_id'] === $my_datas['id']) {

							    	echo $this->Html->link('<i class="fa fa-trash-o"></i> 削除',
							            array(
							                'controller' => 'Readays',
							                'action'     => 'deleteReaday',
							                'rd'         => $key['id'],
							            ),
							             array(
							                'escape'  => false,
							                'confirm' => 'このリーデイを削除します。よろしいですか?',
							            )
						            );
						    	}
						    ?>
						</div>
					</div><!-- readay -->
					<hr>
				<?php endforeach;?>
			<?php else:?>
		    	<div class="text-center">
		    		<p>リーデイはまだありません。</p>
		    	</div>
	    	<?php endif;?>
		</div><!-- col-md-7 -->

		<div class="col-md-5">
			<div class="user_info">
				<div class="well">
					<?php echo 
				      	$this->Html->link(
					      	$this->Html->image(
					      		$users['User']['picture'],array('class' => 'img-rounded')).' '.h($users['User']['name']),
					      	array(
					      		'controller' => 'Mains',
					      		'action'     => 'myReadayShow',
					      		'sd'         => $users['User']['social_id']
					      	),
					      	array(
					      		'escape' => false,
					      		'class'  => 'rollover',
					      	)
				      	);
				    ?>
					<div class="btn-group btn-group-justified">
						<?php 
							echo $this->Html->link('リーデイ'.'<p>'.$readay_sum.'</p>',
								array(
									'controller' => 'Mains',
									'action'     => 'myReadayShow',
									'sd'         => $users['User']['social_id'],
								),
								array(
									'class'  => 'btn btn-default',
									'escape' => false
								)
							);

						    echo $this->Js->link('フォロー'.'<p>'.$followed_sum.'</p>',
						    	array(
						    		'controller' => 'Socials',
						    		'action'     => 'followShow',
						    	    'sd'         => $users['User']['social_id']
						    	),
						    	array(
						    		'update'     => '#readay_box',
						    		'class'      => 'btn btn-default',
						    		'escape'     => false,
						    	)
						    );

						    echo $this->Js->link('フォロワー'.'<p>'.count($users['Follow']).'</p>',
						    	array(
						    		'controller' => 'Socials',
						    		'action'     => 'followerShow',
						    	    'sd'         => $users['User']['social_id']
						    	),
						    	array(
						    		'update'     => '#readay_box',
						    		'class'      => 'btn btn-default',
						    		'escape'     => false,
						    	)
						    );
						?>
					</div>

					<div id="follow_box">
						<?php 
							if($users['User']['id'] !== $my_datas['id']) {
								
								if(!empty($follow_check)) {

									echo $this->Js->link('フォローをはずす',
										array(
											'controller' => 'Socials',
											'action' => 'removeFollow',
											'sd'     => $users['User']['social_id'],
										),
										array(
											'update' => '#follow_box',
											'class'  => 'btn btn-danger btn-block'
										)
									);
								} else {
									echo $this->Js->link('フォローする',
										array(
											'controller' => 'Socials',
											'action' => 'addFollow',
											'sd'     => $users['User']['social_id'],
										),
										array(
											'update' => '#follow_box',
											'class'  => 'btn btn-success btn-block'
										)
									);
								}
							}
						?>
					</div>
				</div><!-- well -->
			</div>
			
	        <div class="col-md-12">
		        <div class="list-group">
					<p>ブックタグ</p>
					<?php echo $this->element('Mains/tag_list');?>  
				</div>
			</div>
	    </div><!-- col-md-5 -->
	</div><!-- row -->
</div><!-- container -->
