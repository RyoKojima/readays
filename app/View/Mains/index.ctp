<p class='lead col-md-12 text-center'>あなたのオススメを教えて?</p>
<div class="text-center error-msg">
    <p><?php echo $this->Session->flash(); ?></p>
</div>

<div id="question_list" class="clearfix">

	<div id="item_wrapper">	
    	<?php foreach ($questions as $key):?>
        	<div class="item-box load-item col-md-4 col-sm-12 col-xs-12">
                <div class="thumbnail">
                    <div class="media">
                        <?php 

                            if($key['Question']['anonymity'] == 0) {
                                echo 
                                $this->Html->link(
                                    $this->Html->image($key['User']['picture'],array('class' => 'img-circle')),
                                    array(
                                        'controller' => 'Mains',
                                        'action'     => 'profileShow',
                                        'nn'         => $key['User']['nickname']
                                    ),
                                    array(
                                        'escape' => false,
                                        'class'  => 'pull-left rollover',
                                    )
                                );
                            }
                        ?>
                       
                        <div class="media-body">                            
                            <p class="media-heading">
                                <?php 
                                    if($key['Question']['anonymity'] == 0) {
                                        echo h($key['User']['name']);
                                    }
                                ?>
                            </p>

                            <p><?php echo $this->Html->link(h($key['Question']['content']), array(
                                        'controller' => 'Questions',
                                        'action'     => 'detailQuestion',
                                        'qd'         => $key['Question']['id']
                                    ));?></p>
                            
                        </div>
                    </div>
                    <div class="caption">

                        <div class="book-thum">
                            <?php foreach ($key['Answere'] as $ans){

                                echo $this->Html->image($ans['book_img']);
                            }?>
                        </div>
                    </div>
                </div>
        	</div>
    	<?php endforeach;?>
	</div>

	<div class="page-nav">
        <?php echo $this->Paginator->next('次のページ',array('class'=>'next')); ?>
    </div>
</div>
