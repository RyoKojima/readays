
<div class="col-md-12 text-center">
	<?php 
		echo $this->Html->image($users['User']['picture'], array('class' => 'img-circle'));
	?>
	<p class="lead"><?php echo h($users['User']['name']).'さんのオススメ';?></p>
</div>

<?php if(empty($ans_data)):?>

	<p class="col-md-12 text-center">オススメの投稿はまだありません。</p>

<?php else:?>
	<div id="item_wrapper" class="clearfix">

		<?php foreach ($ans_data as $key):?>
			<div class="col-md-4 col-sm-12 col-xs-12 item-box load-item">
				<div class="question-content">
					<p><?php echo h($key['Question']['content']);?></p>
				</div>

				<div class="thumbnail ans-card <?php echo $key['Color']['color_name'];?>"> 
				    <p><?php echo 
					      	$this->Html->link(
						      	$this->Html->image($key['User']['picture'],array('class' => 'img-circle')),
						      	array(
						      		'controller' => 'Mains',
							      	'action'     => 'profileShow',
							      	'nn'         => $key['User']['nickname']
						      	),
						      	array(
						      		'escape' => false,
						      		'class'  => 'rollover user-unit',
						      	)
					      	);
					    ?>

					    <span class="pull-right"><?php echo $key['Answere']['created'];?></span>
					</p>

					<div class='card text-center'>
				    	<?php echo $this->Html->image($key['Answere']['book_img']);?>
				    </div>
				      
					<div class="caption text-center">
						<p><?php echo '<i class="fa fa-quote-right"></i> '.h($key['Answere']['phrase']);?></p>
						<p><?php echo '出典:'.h($key['Answere']['page_number']).'ページ';?></p>
						
						<hr>
						
						<div>
							<p>心に響いたユーザー</p>
							<?php 
								if(empty($key['Supporter'])) {
									echo '<h4>0人</h4>';
								} else {

									foreach ($key['Supporter'] as $supporter_data) {
										echo $this->Html->link(
											$this->Html->image($supporter_data['User']['picture'],
												array('class' => 'img-circle')
											),
											array(
		                                    	'controller' => 'Mains',
		                                    	'action'     => 'profileShow',
		                                    	'sd'         => $supporter_data['User']['nickname']
		                                	),
			                                array(
			                                    'escape' => false,
			                                    'class'  => 'rollover user-unit',
			                                )
			                            );
									}
								}
	                        ?>
						</div>

						<hr>

						<a href="<?php echo h($key['Answere']['book_detail']);?>" class="btn btn-default btn-sm" target="_blank">Amazon</a>

						<?php 
							if($key['Answere']['user_id'] === $my_datas['id']) {
								echo $this->Html->link('<i class="fa fa-trash-o"></i>',
									array(
										'controller' => 'Mains',
										'action'     => 'ansDelete',
										'qd'         => $key['Question']['id'],
										'ad'         => $key['Answere']['id']
									),
									array(
										'escape' => false,
										'class'  => 'btn btn-default btn-sm',
										'confirm' => 'このカードを削除しますか?',
									)
								);
							}
						?>
						
					</div>
				</div>
			</div>
		<?php endforeach;?>
		<div class="page-nav">
	    	<?php echo $this->Paginator->next('次のページ',array('class'=>'next')); ?>
		</div>
	</div>
<?php endif;?>

