<?php 
	echo $this->Js->link('全て'.' '.'<span class="badge">'.$book_sum.'</span>',
		array(
		'controller' => 'Books',
		'action'     => 'allBookList',
			'sd'         => $users['User']['social_id']
		),
		array(
			'update'     => '#readay_box',
			'class'      => 'list-group-item',
			'escape'     => false,
		)
	);
?>
<?php foreach ($tags as $key):?>
	
	<?php 
		echo $this->Js->link(h($key['Tag']['tag_name']).' '.'<span class="badge">'.count($key['BooksAtchTag']).'</span>',
			array(
				'controller' => 'Books',
				'action'     => 'bookList',
				'td'         => $key['Tag']['id'],
				'sd'         => $users['User']['social_id']
			),
			array(
				'update'     => '#readay_box',
				'class'      => 'list-group-item',
				'escape'     => false,
			)
		);
	?>
<?php endforeach;?>
			