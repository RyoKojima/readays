<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content text-center">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h1 class="modal-title">開発者</h1>
        </div>
        <div class="modal-body">
          <?php echo $this->Html->image('member1.jpg', 
             array('class' => 'img-circle'));?>
          <h2>小島 凌</h2>
          <P class="lead">東京工科大学4年</P>
          <h3>
            <a target="_blank" href="http://www.facebook.com/ryou.kojima7">
              <i class="fa fa-facebook-square"></i>
            </a>
          </h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h2 class="modal-title">プライバシーポリシー</h2>
        </div>
        <div class="modal-body">
          <p>制定日：2013年10月30日<p>
          <p>最終改訂日：2014年3月24日</p>
          <P>本文書は、当サイト（Readays）における個人情報の保護およびその適切な取り扱いについての方針を示したものです。</P>
          <h2>1.情報の収得</h2>
          <p>当サイトでは、主に次の二つの方法によってユーザの情報を取得することがあります。</p>
          <h3>1.1 登録</h3>
          <p>当サイトでは、FacebookまたはTwitterでご利用されているユーザー名を収集します。この情報は、サービスご利用時に、ご利用者の確認・照会のために使用されます。</p>
          <h3>1.2 クッキー（Cookies）</h3>
          <p>当サイトはコンテンツの充実や安定的なサイト運営を目的に、後述するアクセス解析サービスを利用しています。そのため、クッキーを通じて情報を収集することがあります。クッキーはユーザがサイトを訪れた際に、そのユーザのコンピュータ内に記録されます。ただし、記録される情報には、ユーザ名やメールアドレスなど個人を特定するものは一切含まれません。</p>
          <p>こうしたクッキーによる情報収集を好まない場合、ユーザ自身がブラウザでクッキーの受け入れを拒否するように設定することも可能です。その際はコンテンツによってはサービスが正しく機能しない場合もありますので、あらかじめご了承ください。</p>
          <h2>2.情報の利用</h2>
          <p>上記の方法により収集した情報につきましては、当サイトおよびその提携先において、当サイトのサービス向上や、マーケティング活動の効果追跡のために利用されます。ただし、前述の通り、こうした情報から当サイトの管理人が個人を特定することはできません。法律の適用を受ける場合や法的強制力のある請求以外には、いかなる個人情報も、ユーザ本人の許可なく第三者に開示いたしません。</p>
          <h2>3.注意事項</h2>
          <p>当サイトでは、上記の方法により収集した情報の保護に細心の注意を払っています。ただし、他のユーザが閲覧できるスペースにユーザ自身が自発的に個人情報を入力した場合、意図しない利用がなされる可能性があることに十分に留意してください。</p>
          <h2>4.当サイトが利用・提携しているサービス</h2>
          <p>当サイトは以下のアクセス解析サービスを利用しています。前述の通り、トラフィックデータの収集のためにクッキーが使用されていますが、ユーザはブラウザでクッキーの受け入れを拒否するように設定することが可能です。これらのサービスにおいて取得・収集される情報については、各サービスのプライバシーポリシーをご確認ください。</p>
          <h3>4.1 アクセス解析サービス</h3>
            <a target="_blank" href="http://www.google.co.jp/intl/ja/analytics/">Google アナリティクス</a>
          <h3>4.2 アフィリエイトプログラム</h3>
          <p>当サイトは以下のアフィリエイトプログラムに参加しています。これらのプログラムにおいて取得・収集される情報については、各プログラムのプライバシーポリシーをご確認ください。</p>
          <h4>4.2.1 Amazon アソシエイト・プログラム</h4>
          <p>Readaysは、amazon.co.jp を宣伝しリンクすることによってサイトが紹介料を獲得できる手段を提供することを目的に設定されたアフィリエイト宣伝プログラムである、Amazon アソシエイト・プログラムの参加者です。このプログラムにより、第三者がコンテンツおよび宣伝を提供し、サイトの訪問者から直接情報を収集し、訪問者のブラウザにクッキーを設定したりこれを認識したりする場合があります。</p>
          <h2>5．免責事項</h2>
          <p>Readaysを利用することによって生じた損害に対して一切の責任（間接損害・特別損害・結果的損害及び付随的損害）を負うものではありません。当サイトの利用に関しましては全て最終自己責任で行って頂くようお願いします。</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->