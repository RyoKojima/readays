<a class="add-supporter btn btn-default btn-sm" data-ad="<?php echo h($ans_id);?>"><i class="fa fa-heart"></i></a>

<script>
$(document).on('click', '.add-supporter', function() {
    $(this).addClass('disabled');
    var ans_id = $(this).attr('data-ad');
    var data   = {'ans_id':ans_id};

    $.ajax({
        type: 'GET',
        url: '<?php echo $this->webroot;?>Supporters/addSupporter/',
        data: data,
        success: function(data) {
            $('.support_box'+ans_id).html(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
             notif({
              msg: "データの更新に失敗しました。時間をあけて再度お試しください。",
              type: "error",
              position: "center",
            });
        },
    });
});
</script>
