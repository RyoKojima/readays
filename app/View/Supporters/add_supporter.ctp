<a class="remove-supporter btn btn-default btn-sm" data-ad="<?php echo h($ans_id);?>"><i class="fa fa-heart support-done"></i></a>

<script>
$(document).on('click', '.remove-supporter', function() {
    $(this).addClass('disabled');
    var ans_id = $(this).attr('data-ad');
    var data   = {'ans_id':ans_id};

    $.ajax({
        type: 'GET',
        url: '<?php echo $this->webroot;?>Supporters/removeSupporter/',
        data: data,
        success: function(data) {
            $('.support_box'+ans_id).html(data);
        },
    });
});

</script>
