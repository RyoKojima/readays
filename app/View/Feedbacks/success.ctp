<div class="container">

  　<div class="container text-center">
      <p class="lead">貴重なご意見・ご要望をありがとうございます!<br>
      他にもお気づきなことがあれば、また先ほどのフォームから声をお聞かせください。</p>
      <p class="lead">引き続きReadays(リーデイズ)をよろしくお願い致します。</p>
      <?php 
         echo $this->Html->link('TOPに戻る',
            array(
               'controller' => 'Mains',
               'action'     => 'index',
            )
         );
      ?>
　　</div>
</div>
