<div class="container">
   <h4 class="text-center">サービスに関するご意見・ご要望はこちらのフォームよりご送信ください。</h4>
   　　　<?php
         echo $this->Form->create('Feedback', 
            array( 
               'type'=>'post',
               'url' => array(
                  'controller' => 'Feedbacks',
                  'action'     => 'addFeedBack'
               ),
               'class' => 'form-horizontal',
               'role'  => 'form'
            )
         );
      ?>

      <div class="form-group">
         <?php
            echo $this->Form->input('comment', 
               array(
                'type'        => 'textarea',
                'class'       => 'form-control',
                'placeholder' => 'どんなご意見・ご要望もお聞かせ下さい!!',
                'label'       => false,
               )
            );
         ?>
      </div>

      <div class="text-center">
         <?php
            echo $this->Form->button('フィードバックを送る',
               array(
                  'type'  => 'submit',
                  'class' => 'btn btn-success btn-lg'
               )
            );
         ?>
      </div>

 　　　　<?php echo $this->Form->end(); ?>
</div>