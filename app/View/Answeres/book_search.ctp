<div id="book_result">

	<?php 
		if(!empty($book_xml->Items->Item)) {
			foreach($book_xml->Items->Item as $Item) {
				echo $this->Js->link($this->Html->image($Item->MediumImage->URL),
					array(
						'controller' => 'Answeres',
			     		'action'     => 'ansForm?&asin='.$Item->ASIN.'&qd='.$qd
			   		),
			   		array(
			     		'update' => '#s_content',
			     		'escape' => false
			   		)
			 	);
			}
		} else {
			echo '<div class="text-center"><p>データが見つかりません。</p></div>';
		} 
	?>
</div>

<script>
 $('#book_result a').click(function(){
    $('#book_result').html('<div class="loder-img text-center"><img src="<?php echo $this->webroot;?>img/loader.gif"></div>');
  });
</script>

<?php echo $this->Js->writeBuffer(); ?>