<!-- Form -->
<div class="container">
    <div class="col-sm-12 text-center ans-img">
       <?php echo $this->Html->image($book_xml->Items->Item->MediumImage->URL);?>
    </div>
    <div class="col-sm-12">
        
        <?php 
            echo $this->Form->create('Answere', 
            array( 
               'type'=> 'post',
               'url' => array(
                    'controller' => 'Answeres',
                    'action'     => 'addAns',
                ), 
               'id'    => 'addAnsForm',
               'class' => 'form-horizontal',
               'role'  => 'form'));
        ?>

            <div class="form-group">
                <?php
                    echo $this->Form->input('phrase', 
                        array(
                           'type'        => 'textarea',
                           'class'       => 'form-control',
                           'placeholder' => '相談者の心に響きそうな、または自分の好きなフレーズも教えてあげよう!',
                           'rows'        => '10',
                           'label'       => false,
                        )
                    );
                ?>
            </div>

            <div class="form-group required">
                <?php
                 echo $this->Form->input('page_number', 
                    array(
                       'type'        => 'number',
                       'class'       => 'form-control',
                       'min'         => 1,
                       'placeholder' => 'ページ番号',
                       'label'       => false,
                     )
                 );
                ?>
            </div>

            <div class="form-group">
                <label for="BookColor">カードカラーを選択</label>
                <?php
                   echo $this->Form->input('color', 
                      array(
                         'type'    => 'select',
                         'options' => $color_list,
                         'class'   => 'form-control',
                         'label'   => false,
                       )
                   );
                ?>
            </div>
            
            <?php
                echo $this->Form->input('book_img', 
                    array(
                        'type'  => 'hidden',
                        'class' => 'form-control',
                        'value' => $book_xml->Items->Item->MediumImage->URL,
                    )
                );

                echo $this->Form->input('book_detail', 
                    array(
                        'type'  => 'hidden',
                        'class' => 'form-control',
                        'value' => $book_xml->Items->Item->DetailPageURL,
                    )
                );
            
                echo $this->Form->input('qd', 
                    array(
                         'type'  => 'hidden',
                         'class' => 'form-control',
                         'value' => $questions['Question']['id'],
                    )
                );
            ?>

           <div class="form-group">
           
            <?php
                 echo $this->Form->button('オススメする', 
                    array(
                       'type'  => 'submit',
                       'id'    => 'addAnswere',
                       'class' => 'btn btn-success btn-block'
                    )
                 );
             ?>
           </div>
           <?php echo $this->Form->end(); ?>
       
    </div>
</div>

<script>
$(document).ready(function(){
    $("#addAnsForm").validate({});
});
</script>