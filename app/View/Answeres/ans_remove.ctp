<button class="add-stock btn btn-default btn-sm" data-ad="<?php echo h($ans_id);?>"><i class="fa fa-star"></i></button>

<script>
    $(document).on('click', '.add-stock', function() {
        $(this).addClass('disabled');
        var ans_id = $(this).attr('data-ad');
        var data   = {'ans_id':ans_id};

        $.ajax({
            type: 'GET',
            url: '<?php echo $this->webroot;?>Answeres/ansStock/',
            data: data,
            success: function(data) {

                $('.stock_box'+ans_id).html(data);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

                notif({
                  msg: "データの更新に失敗しました。時間をあけて再度お試しください。",
                  type: "error",
                  position: "center",
                });
            },
        });
    });
</script>
