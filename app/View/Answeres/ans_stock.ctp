<a class="remove-stock btn btn-default btn-sm" data-ad="<?php echo h($ans_id);?>"><i class="fa fa-star stock-done"></i></a>

<script>
    $(document).on('click', '.remove-stock', function() {
        $(this).addClass('disabled');
        var ans_id = $(this).attr('data-ad');
        var data   = {'ans_id':ans_id};

        $.ajax({
            type: 'GET',
            url: '<?php echo $this->webroot;?>Answeres/ansRemove/',
            data: data,
            success: function(data) {
                $('.stock_box'+ans_id).html(data);
            },
        });
    });
</script>

