<!DOCTYPE html>
<html　lang="ja" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <meta property="fb:admins" content="ryou.kojima7"/>
    <meta property="og:title"content="Readays"/>
    <meta property="og:type"content="website"/>
    <meta property="og:description"content="あなたの悩みを解決する本にすぐに出会えるQ＆Aブックサービス"/>
    <meta property="og:url"content="http://readays.krmtd.com/"/>
    <meta property="og:image"content="http://readays.krmtd.com/img/thum-readays.png"/>
    <meta property="og:site_name"content="Readays"/>
    <meta property="fb:app_id"content="535730519848826"/>
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" 
    type="text/css" href="http://mplus-fonts.sourceforge.jp/webfonts/mplus_webfonts.css" />

    <title>Readays</title>

    <?php echo $this->Html->css('bootstrap/bootstrap.min.css'); ?>
    <?php echo $this->Html->css('landing.css'); ?>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-41399872-4', 'krmtd.com');
    ga('send', 'pageview');
    </script>
  </head>

    <body>
        <nav class="navbar global-nav" role="navigation" data-target="#bs-example-navbar-collapse-1">
            <div class="container">
                <div class="navbar-header">

                    <?php
                        echo $this->Html->link('Readays',
                            array(
                                'controller' => 'Mains',
                                'action'     => 'index'
                            ),
                            array(
                                'id'    => 'brand',             
                                'class' => 'navbar-brand'
                            )
                        );
                    ?>

                </div>

                <div class="navbar-right">

                    <div class="navbar-text">
                      <div class="fb-like" data-href="http://readays.krmtd.com/" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                
                      <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://readays.krmtd.com/" data-text="あなたの悩みを解決する本にすぐに出会えるQ＆Aブックサービス" data-lang="ja">ツイート</a>
                    </div>

                    <div id="fb-root"></div>
                </div>

            </div>
        </nav>


        <?php echo $content_for_layout; ?>

        <footer>
            <div class="container">
                <p class="text-muted credit">&copy; 2013 Readays. &middot; <a data-toggle="modal" href="#myModal">開発者</a> &middot; 
                <a data-toggle="modal" href="#myModal2">プライバシーポリシー</a></p>
            </div>
        </footer>
    

   <?php echo $this->element('documents/readays_doc');?>

   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   <?php echo $this->Html->script("bootstrap.min.js"); ?>
   <script>
       
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1&appId=591472070896591";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  　　!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
   </script>
   
   <?php echo $this->Js->writeBuffer( array( 'inline' => 'true')); ?>
  </body>
</html>