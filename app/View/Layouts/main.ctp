<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <meta property="fb:admins" content="ryou.kojima7"/>
        <meta property="og:title"content="Readays"/>
        <meta property="og:type"content="website"/>
        <meta property="og:description"content="みんなのオススメ本があつまるソーシャルブックサービス"/>
        <meta property="og:url"content="http://readays.krmtd.com/"/>
        <meta property="og:image"content="http://readays.krmtd.com/img/thum-readays.png"/>
        <meta property="og:site_name"content="Readays"/>
        <meta property="fb:app_id"content="535730519848826"/>
    
        <title>Readays</title>

        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css('bootstrap/bootstrap.min.css'); ?>
        <?php echo $this->Html->css('notifIt.css');?>
        <?php echo $this->Html->css('main.css');?>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="http://mplus-fonts.sourceforge.jp/webfonts/mplus_webfonts.css" />

        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41399872-4', 'krmtd.com');
        ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div class="navbar global-nav navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span><i class="fa fa-bars"></i></span>
                    </button>
                    <?php
                        echo $this->Html->link('Readays',
                            array(
                                'controller' => 'Mains',
                                'action'     => 'index'
                            ),
                            array(
                                'id'    => 'brand',             
                                'class' => 'navbar-brand'
                            )
                        );
                    ?>
                </div>
                <div class="collapse navbar-collapse">
                    <?php if(empty($my_datas)):?>
                        <p class="navbar-text navbar-right">
                            <?php 
                                echo $this->Html->link('<i class="fa fa-facebook"></i> Login with Facebook',
                                    array(
                                        'controller' => 'auth',
                                        'action'     => 'facebook'
                                    ),
                                    array(
                                        'class'  => 'navbar-link',
                                        'escape' => false
                                    )
                                );
                            ?>
                        </p>

                        <p class="navbar-text navbar-right">
                            <?php 
                                echo $this->Html->link('<i class="fa fa-twitter"></i> Login with Twitter',
                                    array(
                                        'controller' => 'auth',
                                        'action'     => 'twitter'
                                    ),
                                    array(
                                        'class'  => 'navbar-link',
                                        'escape' => false
                                    )
                                );
                            ?>
                        </p>
                    <?php else:?>
                    <ul class="nav navbar-nav pull-right">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#addQuestion">
                            <i class="fa fa-pencil-square-o"></i> みんなに相談する</a>
                        </li>
                        <li id="response_box" class="dropdown">
                            <?php if(count($res_link_data) > 0):?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i> レスポンス 
                                    <span class="badge"><?php echo count($res_link_data);?></span>
                                </a>
                            <?php else:?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> レスポンス</a>
                            <?php endif;?>

                            <ul class="dropdown-menu">
                                <?php if(empty($res_link_data)):?>

                                    <li><a href='#'>お知らせはありません。</a></li>
                                <?php else: ?>
                                    <?php foreach ($res_link_data as $key):?>
                                        <li class="text-center">
                                            <?php 
                                                echo 
                                                $this->Html->link('<p>'.$key['QuestionResponse']['created'].'</p>'.$this->Html->image($key['User']['picture'],array('class' => 'img-circle')).'<p>'.h($key['QuestionResponse']['message']).'</p>',
                                                    array(
                                                        'controller' => 'Responses',
                                                        'action'     => 'responseCheck',
                                                        'rd'         => $key['QuestionResponse']['id'],
                                                    ),
                                                    array(
                                                        'escape' => false,
                                                        'class' => 'user-unit'
                                                    )
                                                );
                                            ?>
                                        </li>
                                        <li class="divider"></li>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> アカウント <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                  <?php echo $this->Html->link('<i class="fa fa-user"></i> プロフィール',
                                      array(
                                          'controller' => 'Mains',
                                          'action'     => 'profileShow',
                                          'nn'         => $my_datas['nickname']
                                      ),
                                      array(
                                          'escape' => false,
                                      )
                                    );
                                  ?>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <?php 
                                        echo $this->Html->link('<i class="fa fa-pencil-square-o"></i> 投稿した質問',
                                            array(
                                                'controller' => 'Questions',
                                                'action'     => 'myQuestionList',
                                            ),
                                            array(
                                                'escape' => false,
                                            )
                                        );
                                    ?>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <?php 
                                        echo $this->Html->link('<i class="fa fa-reply"></i> フィードバック',
                                            array(
                                                'controller' => 'Feedbacks',
                                                'action'     => 'addFeedBack',
                                            ),
                                            array(
                                                'escape' => false,
                                            )
                                        );
                                    ?>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <?php 
                                        echo $this->Html->link('<i class="fa fa-frown-o"></i> 退会する',
                                            array(
                                                'controller' => 'Users',
                                                'action'     => 'setingShow',
                                            ),
                                            array(
                                                'escape' => false,
                                            )
                                        );
                                    ?>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <?php 
                                        echo $this->Html->link('<i class="fa fa-power-off"></i> ログアウト',
                                            array(
                                                'controller' => 'Users',
                                                'action'     => 'logOut',
                                            ),
                                            array(
                                                'escape' => false,
                                            )
                                        );
                                    ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <?php endif;?>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container">
            <div id="main_content">
                <?php echo $content_for_layout; ?>
            </div>
        </div>

        <footer>
            <div class="container">
                <p class="text-muted credit">&copy; 2013 Readays. &middot; <a data-toggle="modal" href="#myModal">開発者</a> &middot; 
                <a data-toggle="modal" href="#myModal2">プライバシーポリシー</a></p>
            </div>
        </footer>

        <!-- addQuestionModal -->
        <div class="modal fade" id="addQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">みんなに相談</h4>
                    </div>

                    <div class="modal-body">
                        <div class="container">
                            
                            <?php 
                                echo $this->Form->create('Question', 
                                    array( 
                                        'type'=>'post', 
                                        'url' => array(
                                              'controller' => 'Questions',
                                              'action'     => 'addQuestion',
                                        ),
                                        'id'    => 'addQuestionForm', 
                                        'class' => 'form-horizontal',
                                        'role'  => 'form'
                                    )
                                );
                            ?>

                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('content', 
                                        array(
                                           'type'        => 'textarea',
                                           'class'       => 'form-control',
                                           'rows'        => 6,
                                           'placeholder' => 'どんなことに悩んでいるの?詳しく聞かせて?',
                                           'label'       => false,
                                         )
                                    );
                                ?>
                            </div>

                            <div class="form-group">
                                <?php 
                                    echo $this->Form->input('anonymity',
                                        array(
                                            'type'  => 'checkbox',
                                            'label' => '匿名で質問する',
                                        )
                                    );
                                ?>
                            </div>

                            <div class="form-group">
                                <?php
                                    echo $this->Form->button('悩みを投稿する', 
                                        array(
                                            'type'  => 'submit',
                                            'class' => 'btn btn-success btn-block'
                                        )
                                    );
                                ?>
                            </div>

                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- loginLinkModal -->
        <div class="modal fade" id="loginLink" tabindex="-1" role="dialog" aria-labelledby="loginLinkLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="loginLinkLabel">ログイン</h4>
                    </div>

                    <div class="modal-body">
                        <div class="container">
                            <p class="lead">この機能を実行するにはログインが必要です。</p>

                            <?php echo $this->Html->link('Login with Facebook',
                                array(
                                    'controller' => 'auth', 
                                    'action' => 'facebook'
                                ),
                                array(
                                    'class' => 'btn btn-primary btn-lg')
                                );
                            ?>

                            <?php echo $this->Html->link('Login with Twitter',
                                array(
                                    'controller' => 'auth', 
                                    'action' => 'twitter'
                                ),
                                array(
                                    'class' => 'btn btn-info btn-lg')
                                );
                            ?>
                            
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php echo $this->element('documents/readays_doc');?>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <?php echo $this->Html->script("bootstrap.min.js"); ?>
        <?php echo $this->Html->script("masonry.pkgd.min.js"); ?>
        <?php echo $this->Html->script("jquery.infinitescroll.min.js"); ?>
        <?php echo $this->Html->script("jquery.validate.min.js"); ?>
        <?php echo $this->Html->script("messages_ja.js"); ?>
        <?php echo $this->Html->script("notifIt.js"); ?>
        <?php echo $this->Js->writeBuffer(); ?>
        <script>
            $(function() {

                $("#addQuestionForm").validate({});

                $(window).load(function(){
                    var $container = $('#item_wrapper');

                    $container.masonry({
                        columnWidth: '.item-box',
                        itemSelector: '.item-box'
                    });

                    $container.infinitescroll({
                        navSelector : '.page-nav',
                        nextSelector : '.page-nav a',
                        itemSelector : '.load-item',
                        loading : {
                            msgText: "",
                            finishedMsg: "",
                            img : "<?php echo $this->webroot;?>img/loader.gif"
                        },
                    },
                    function( newElements ) {
                        var $newElems = $( newElements );
                        $container.masonry('appended',$newElems,true);
                    });
                });

                $(document).on('click', '.add-supporter', function() {
                    $(this).addClass('disabled');
                    var ans_id = $(this).attr('data-ad');
                    var data   = {'ans_id':ans_id};

                    $.ajax({
                        type: 'GET',
                        url: '<?php echo $this->webroot;?>Supporters/addSupporter/',
                        data: data,
                        success: function(data) {
                            $('.support_box'+ans_id).html(data);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                             notif({
                              msg: "データの更新に失敗しました。時間をあけて再度お試しください。",
                              type: "error",
                              position: "center",
                            })
                        },
                    });
                });

                $(document).on('click', '.remove-supporter', function() {
                    $(this).addClass('disabled');
                    var ans_id = $(this).attr('data-ad');
                    var data   = {'ans_id':ans_id};

                    $.ajax({
                        type: 'GET',
                        url: '<?php echo $this->webroot;?>Supporters/removeSupporter/',
                        data: data,
                        success: function(data) {
                            $('.support_box'+ans_id).html(data);
                        },
                    });
                });
       
                $('#book_search').click(function(){
                    $('#s_content').html('<div class="loder-img text-center"><img src="<?php echo $this->webroot;?>img/loader.gif"></div>');
                });
            });
        </script>

   <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1&appId=591472070896591";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  　　!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
   </script>
   
    </body>
</html>